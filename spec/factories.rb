FactoryGirl.define do
	factory :user do
		name "Austin Brennan"
		email "awbrenn@clemson.edu"
		password "foobar"
		password_confirmation "foobar"
	end
end